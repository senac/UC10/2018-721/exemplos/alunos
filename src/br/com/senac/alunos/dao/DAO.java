package br.com.senac.alunos.dao;

import java.util.List;

public interface DAO<T> {

    void inserir(T objeto);

    void atualizar(T objeto);

    void delete(int id);

    List<T> listaTodos();

    T buscarPorId(int id);

}
