package br.com.senac.alunos.dao;

import br.com.senac.alunos.model.Aluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlunoDAO implements DAO<Aluno> {

    @Override
    public void inserir(Aluno aluno) {
        Connection connection = null;
        try {
            String query = "INSERT INTO ALUNO (NOME , TELEFONE , EMAIL , TURMA) VALUES (?, ? , ? , ?)";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, aluno.getNome());
            ps.setString(2, aluno.getTelefone());
            ps.setString(3, aluno.getEmail());
            ps.setString(4, aluno.getTurma());
            int linhas = ps.executeUpdate();
            if (linhas > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                aluno.setId(id);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void atualizar(Aluno aluno) {

        Connection connection = null;
        try {
            String query = "UPDATE ALUNO SET NOME= ? ,TELEFONE = ? , EMAIL = ? , TURMA = ? WHERE ID = ? ";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, aluno.getNome());
            ps.setString(2, aluno.getTelefone());
            ps.setString(3, aluno.getEmail());
            ps.setString(4, aluno.getTurma());
            ps.setInt(5, aluno.getId());
            ps.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Aluno> listaTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Aluno buscarPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Aluno buscarPorNome(String nome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
